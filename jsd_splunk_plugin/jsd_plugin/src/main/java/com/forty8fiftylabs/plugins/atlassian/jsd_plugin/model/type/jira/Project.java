package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Project {
	project_created,
	project_updated,
	project_deleted
}
