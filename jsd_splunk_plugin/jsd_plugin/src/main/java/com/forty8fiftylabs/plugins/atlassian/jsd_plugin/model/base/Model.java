package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base;

public interface Model {
	public int getId();

	public void setId(int id);
}
