package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto;

import java.util.List;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Links;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Project;

public class ProjectResponse {
	private int size;
	private int start;
	private int limit;
	private boolean isLastPage;
	private List<Project> values;
	
	public Links _links;
	
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public boolean getIsLastPage() {
		return isLastPage;
	}
	public void setIsLastPage(boolean isLastPage) {
		this.isLastPage = isLastPage;
	}
	public List<Project> getValues() {
		return values;
	}
	public void setValues(List<Project> values) {
		this.values = values;
	}
}
