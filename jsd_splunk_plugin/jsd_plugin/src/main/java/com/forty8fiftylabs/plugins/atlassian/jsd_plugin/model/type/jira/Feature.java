package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Feature {
	option_voting_changed,
	option_watching_changed,
	option_unassigned_issues_changed,
	option_subtasks_changed,
	option_attachments_changed,
	option_issuelinks_changed,
	option_timetracking_changed
}
