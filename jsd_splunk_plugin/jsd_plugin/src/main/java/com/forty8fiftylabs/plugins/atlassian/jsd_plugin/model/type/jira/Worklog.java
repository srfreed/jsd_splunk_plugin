package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Worklog {
	worklog_created,
	worklog_updated,
	worklog_deleted
}
