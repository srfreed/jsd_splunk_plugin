package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.dao;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.http.NameValuePair;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class used accessing the CHUB Database.
 * Each method leverages generic types and uses the Entity Manager 
 * defined in the Spring Context.
 * 
 */
@Component
public class EntityDao {
	
	private static final Logger logger = Logger.getLogger(EntityDao.class);

	@PersistenceContext
	private EntityManager em;

	/**
	 * Get an entity by its primary key ID.
	 *
	 * @param <T> 			the generic type
	 * @param clazz         The type of entity to retrieve.
	 * @param id            The primary key ID of the entity.
	 * @return 				The entity, or <code>null</code> if none exists.
	 * @throws 				Exception the exception thrown from the find call
	 */
	public <T> T getEntityById(Class<T> clazz, int id) {
		T entity = null;
		try {
			entity = em.find(clazz, id);
		} catch (Exception e) {
			logger.error("Couldn't find " + clazz.getName() + " " + id, e);
			throw e;
		}

		return entity;
	}
	
	/**
	 * Get an entity by its primary key ID.
	 *
	 * @param <T> 			the generic type
	 * @param clazz         The type of entity to retrieve.
	 * @param id            The primary key ID of the entity.
	 * @return 				The entity, or <code>null</code> if none exists.
	 * @throws 				Exception the exception thrown from the find call
	 */
	@SuppressWarnings("unchecked")
	public <T> Collection<T> getAll(Class<T> clazz) {
		Collection<T> entities = null;
		try {
			Query query = em.createQuery("SELECT e FROM "+clazz.getName()+" e");
			entities = (Collection<T>) query.getResultList();
		} catch (Exception e) {
			logger.error("Couldn't find all " + clazz.getName() , e);
			throw e;
		}

		return entities;
	}	

	/**
	 * Add an entity to the database.
	 *
	 * @param <T> 			the generic type
	 * @param entity        The entity to persist.
	 * @return 				The persisted entity.
	 * @throws 				Exception the exception thrown from the persist call
	 */
	@Transactional
	public <T> T addEntity(T entity) throws Exception {
		try {
			em.persist(entity);
		} catch (Exception e) {
			logger.error("Couldn't add " + entity, e);
			throw e;
		}

		return entity;
	}

	/**
	 * Delete an entity from the database.
	 *
	 * @param <T> 			the generic type
	 * @param entity        The entity to remove.
	 * @throws 				Exception the exception thrown from the delete call
	 */
	@Transactional
	public <T> void deleteEntity(T entity) throws Exception {
		if (entity == null) {
			return;
		}
		
		try {
			em.flush();
			em.remove(entity);
		} catch (Exception e) {
			logger.error("Couldn't delete " + entity.getClass().getName(), e);
			throw e;
		}
	}

	/**
	 * Update an entity in the database.
	 *
	 * @param <T> 			the generic type
	 * @param entity        The entity to update.
	 * @return 				The updated entity.
	 * @throws 				Exception the exception from the Merge call
	 */
	@Transactional
	public <T> T updateEntity(T entity) throws Exception {
		try {
			entity = em.merge(entity);
			em.flush();
		} catch (Exception e) {
			logger.error("Couldn't update " + entity.getClass().getName(), e);
			throw e;
		}

		return entity;
	}

	/**
	 * Tries to insert entity in the database. If an error is thrown, it will
	 * update the entity instead
	 *
	 * @param <T> 			the generic type
	 * @param entity        The entity to update.
	 * @return 				The inserted / updated entity.
	 * @throws 				Exception the exception from the update call
	 */
	public <T> T mergeEntity(T entity) throws Exception {
		try {
			this.addEntity(entity);
		} catch (Exception e) {
			this.updateEntity(entity);
			throw e;
		}

		return entity;
	}

	/**
	 * Flush entity manager.
	 */
	public void flushEntityManager() {
		if (em != null) {
			em.getTransaction().begin();
			em.flush();
			em.getTransaction().commit();
		}
	}

	/**
	 * Retrieve by.
	 *
	 * @param <T> 		the generic type
	 * @param field 	the field to search
	 * @param value 	the value to search for in the field
	 * @param clazz 	the clazz representing the entity to search
	 * @return the list
	 */
	public <T> List<T> retrieveBy(String field, String value, Class<T> clazz) {
		List<T> retVal = null;

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = cb.createQuery(clazz);

		Root<T> root = criteriaQuery.from(clazz);
		Path<String> fieldPath = root.get(field);
		criteriaQuery.select(root).where(cb.equal(fieldPath, value));

		TypedQuery<T> query = em.createQuery(criteriaQuery);

		retVal = query.getResultList();

		return retVal;
	}

	/**
	 * Retrieve attribute list.
	 *
	 * @param <T> 		the generic type
	 * @param id 		the id of the Entity to match
	 * @param field 	the field the field in the entity to retrieve the values
	 * @param clazz 	the clazz of the entity to search
	 * @return 			the list of entities from the field 
	 * @throws 			Exception from Entity Manager if there was a problem searching, 
	 *                  or from a Reflection Error getting the values from the entity
	 */
	@SuppressWarnings("unchecked")
	public <T> Set<T> retrieveAttributeList(String id, String field, Class<T> clazz) throws Exception {
		Set<T> retVal = null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = cb.createQuery(clazz);

		Root<T> root = criteriaQuery.from(clazz);
		Path<String> fieldPath = root.get("id");
		criteriaQuery.select(root).where(cb.equal(fieldPath, id));

		TypedQuery<T> query = em.createQuery(criteriaQuery);

		T u = query.getSingleResult();

		try {
			Class<?> c = Class.forName(clazz.getName());
			Method method = c.getDeclaredMethod("get" + field.substring(0, 1).toUpperCase() + field.substring(1), new Class[] {});
			retVal = (Set<T>) method.invoke(u, new Object[] {});
		} catch (Exception e) {
			logger.error("Couldn't get " + clazz.getName(), e);
			throw e;
		} 
		
		return retVal;
	}

	/**
	 * Retrieve by.
	 *
	 * @param <T> 		the generic type
	 * @param params 	a list of NameValuePair objects that are used to perform the search
	 * @param clazz 	the clazz of the Entity to search for matches from the KeyValuePairs
	 * @return 			the list of Entities that match the search values
	 */
	public <T> List<T> retrieveBy(List<NameValuePair> params, Class<T> clazz) {
		List<T> retVal = null;
		
		CriteriaBuilder cb = this.em.getCriteriaBuilder();
		
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		Root<?> root = cq.from(clazz);
		
		Predicate p = null;
		for(NameValuePair v: params){
			Path<String> fieldPath = root.get(v.getName());
			p = cb.equal(fieldPath, v.getValue());
		}
		cq.where(p);
		
		TypedQuery<T> query = em.createQuery(cq);

		retVal = query.getResultList();
		
		return retVal;
	}
}
