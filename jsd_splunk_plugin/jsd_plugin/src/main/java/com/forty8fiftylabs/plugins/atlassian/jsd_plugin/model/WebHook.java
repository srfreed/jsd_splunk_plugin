package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.Model;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.ModelBase;

@Entity
@Table(name="WebHook")
public class WebHook extends ModelBase implements Model, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "WEBHOOK_ID")
	private int id;
	
	@Column(name = "WEBHOOK_NAME")
	private String name;
	
	@Column(name = "WEBHOOK_URL")
	private String URL;
	
	@ElementCollection
	@CollectionTable(name = "WebHookEvents", joinColumns = @JoinColumn(name="WEBHOOK_ID"))
	@Column(name = "WEBHOOK_EVENT")
	private Collection<String> events;
	
	@Column(name = "WEBHOOK_CLIENT_KEY")
	private String clientKey;
	
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name="PR_ID")
	private ProjectRegistration projectRegistration;
	
	public WebHook() {}
	
	public WebHook(int id){
		this.id = id;
	}
	
	
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public Collection<String> getEvents() {
		return events;
	}

	public void setEvents(Collection<String> events) {
		this.events = events;
	}

	public String getClientKey() {
		return clientKey;
	}
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public ProjectRegistration getProjectRegistration() {
		return projectRegistration;
	}

	public void setProjectRegistration(ProjectRegistration projectRegistration) {
		this.projectRegistration = projectRegistration;
	}	
}
