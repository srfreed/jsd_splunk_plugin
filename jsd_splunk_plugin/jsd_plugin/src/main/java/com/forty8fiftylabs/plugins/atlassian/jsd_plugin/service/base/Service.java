package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.base;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;

public interface Service<T> {

	ResponseWrapperDTO create(T m) throws Exception;

	ResponseWrapperDTO update(T m) throws Exception;

	ResponseWrapperDTO delete(T m) throws Exception;

	ResponseWrapperDTO retrieve(T m) throws Exception;
	
	ResponseWrapperDTO retrieveBy(String field, String value) throws Exception;
	
	ResponseWrapperDTO retrieveList(String id, String field) throws Exception;
	
	ResponseWrapperDTO retrieveQuery(String qs) throws Exception;
	
	T getModel();

	void setModel(T model);
}