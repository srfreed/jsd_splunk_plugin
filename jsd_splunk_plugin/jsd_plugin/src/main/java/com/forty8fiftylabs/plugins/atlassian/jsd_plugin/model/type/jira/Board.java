package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Board {
	board_created,
	board_updated,
	board_deleted,
	board_configuration_changed
}
