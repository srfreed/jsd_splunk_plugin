package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.Model;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.ModelBase;

@Entity
@Table(name="Projects")
public class Project extends ModelBase implements Model, Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PROJECT_ID")
	private int id;
	
	@Column(name = "PROJECT_PROJECT_ID")
	private int projectId;
	
	@Column(name = "PROJECT_NAME")
	private String projectName;
	
	@Column(name = "PROJECT_KEY")
	private String projectKey;
	
	@Column(name = "PROJECT_CLIENT_KEY")
	private String clientKey;
	
	@OneToOne(mappedBy = "project", optional = false, fetch=FetchType.EAGER)
	private ProjectRegistration registration;
	
	@OneToOne(cascade=CascadeType.ALL, optional = true, fetch=FetchType.EAGER)
	@JoinColumn(name="LINK_ID")
	public Links _links;
	
	public Project() {}
	public Project(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectKey() {
		return projectKey;
	}
	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}
	public String getClientKey() {
		return clientKey;
	}
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}
}
