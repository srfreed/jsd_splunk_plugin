package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.controller.webhooks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.IgnoreJwt;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.WebHookMessage;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityService;


@RestController
@RequestMapping(value="/webhooks/message")
public class WebhookMessageController {
	
	@Autowired
	EntityService service;
	
	@IgnoreJwt
    @RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseStatus( HttpStatus.CREATED )
    public @ResponseBody ResponseWrapperDTO add(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody String message) throws Exception {
		WebHookMessage webHookMessage = new WebHookMessage(message);
		ResponseWrapperDTO retval = service.create(webHookMessage, WebHookMessage.class);
        return retval;
    }
}
