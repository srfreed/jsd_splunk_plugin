package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Comment {
	comment_created,
	comment_updated,
	comment_deleted
}
