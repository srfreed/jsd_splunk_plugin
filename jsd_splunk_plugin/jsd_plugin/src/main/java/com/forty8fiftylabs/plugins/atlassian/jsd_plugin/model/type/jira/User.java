package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum User {
	user_created,
	user_updated,
	user_deleted
}
