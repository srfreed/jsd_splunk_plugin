package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Issue {
	issue_created,
	issue_updated,
	issue_deleted,
	worklog_updated
}
