package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponseWrapperDTO {

	private Integer responseCode;
	private List<String> message = new ArrayList<String>();
	private Object payload;
	private Throwable exception;
	private String location;
	
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public List<String> getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message.add(message);
	}
	public Object getPayload() {
		return payload;
	}
	public void setPayload(Object payload) {
		this.payload = payload;
	}
	public Throwable getException() {
		return exception;
	}
	public void setException(Throwable exception) {
		this.exception = exception;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
