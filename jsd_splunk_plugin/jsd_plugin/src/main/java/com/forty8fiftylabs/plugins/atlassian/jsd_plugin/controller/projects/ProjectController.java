package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.controller.projects;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.connect.spring.IgnoreJwt;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Project;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityListService;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@Api(value="Project", description="Operations pertaining to JSD Projects")
@RequestMapping(value="/project")
public class ProjectController {
	
	@Autowired
	EntityService service;
	
	@Autowired
	EntityListService listService;

	@IgnoreJwt
	@ApiOperation(value = "Retrieve a list of current Projects for a specific client")
    @RequestMapping(value = "/clientKey/{clientKey}", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody ResponseWrapperDTO retrieveAll(
    		@ApiParam(name="clientKey", value="The clientKey for the Request", required=true) @PathVariable String clientKey, HttpServletResponse response) throws Exception {
		ResponseWrapperDTO retval = listService.retrieveAllProjectsByClientId(prepProject(new Project(), clientKey));
		
		
		response.setStatus(HttpServletResponse.SC_OK);

		return retval;
    }
	
	@IgnoreJwt
	@ApiOperation(value = "Retrieve a Specific Project by Project Id")
    @RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseWrapperDTO retrieve(
    		@ApiParam(name="clientKey", value="The clientKey for the Request", required=true) @PathVariable String clientKey,
    		@ApiParam(name="id", value="The Id of the Registration to retrieve", required=true) @PathVariable int id, HttpServletResponse response) throws Exception {
		ResponseWrapperDTO retval = service.retrieve(prepProject(new Project(id), clientKey), Project.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
        return retval;
    }	

	
	private Project prepProject(Project project, String clientKey){
		if(clientKey!=null && !clientKey.equals("")){
			project.setClientKey(clientKey);
		}
		return project;
	}
}
