package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type;

public enum Software {
	JIRA,
	CONFLUENCE,
	JSD,
	BAMBOO,
	CRUCIBLE,
	HIPCHAT
}
