package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.Model;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.ModelBase;

@Entity
@Table(name="Links")
public class Links extends ModelBase implements Model, Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LINK_ID")
	private int id;
	
	@Column(name = "LINK_BASE")
	private String base;
	
	@Column(name = "LINK_CONTEXT")
	private String context;
	
	@Column(name = "LINK_SELF")
	private String self;
	
	@Override
	public int getId() {
		return id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
	}
	
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getSelf() {
		return self;
	}
	public void setSelf(String self) {
		this.self = self;
	}
}
