package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Version {
	version_released,
	version_unreleased,
	version_created,
	version_moved,
	version_updated,
	version_deleted,
	version_merged
}
