package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.controller.config;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Project;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Splunk;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ProjectResponse;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityListService;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityService;

@Controller
@RequestMapping(value = "/plugin")
public class ConfigController {

	private static String serviceDeskURI = "/rest/servicedeskapi/servicedesk";
	@Autowired
	EntityService service;

	@Autowired
	EntityListService listService;

	@Autowired
	private RestTemplate restTemplate;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showConfig(@AuthenticationPrincipal AtlassianHostUser hostUser, Model model, HttpServletRequest request) throws Exception {
	
		
		model.addAttribute("jsUrl", model.asMap().get("atlassian-connect-all-js-url"));
		model.addAttribute("clientKey", hostUser.getHost().getClientKey());
		model.addAttribute("hostUser", hostUser);
		
		String baseURL = hostUser.getHost().getBaseUrl();
		String contentUrl = String.format("%s" + serviceDeskURI, baseURL);
		String projects = restTemplate.getForObject(contentUrl, String.class);
		
		ObjectMapper mapper = new ObjectMapper();
		ProjectResponse response = mapper.readValue(projects, ProjectResponse.class);
		response.getValues().forEach(new Consumer<Project>() {

		List<Project> projects = new ArrayList<Project>();
			@Override
			public void accept(Project t) {
				try {
					t.setId(t.getProjectId());
					t.setClientKey(hostUser.getHost().getClientKey());
					System.out.println("Trying to Register Project: " + t.getProjectId());
					if (service.retrieve(t, Project.class).getPayload() == null) {
						System.out.println("	Project Does not exist - Creating: " + t.getProjectId());
						projects.add((Project) service.create(t, Project.class).getPayload());
					} else {
						System.out.println("	Project Exist - Updating: " + t.getProjectId());
						projects.add((Project) service.update(t, Project.class).getPayload());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
		
		model.addAttribute("projectList", projects);
		
		List<Splunk> splunks = (List<Splunk>) listService.retrieveAllSplunksByClientId(prepSplunk(new Splunk(),hostUser.getHost().getClientKey())).getPayload();
		model.addAttribute("splunkList", splunks);
		
		return "config";
	}
	
	private Splunk prepSplunk(Splunk splunk, String clientKey){
		if(clientKey!=null && !clientKey.equals("")){
			splunk.setClientKey(clientKey);
		}
		return splunk;
	}
}
