package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.controller.webhooks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.IgnoreJwt;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.WebHook;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityListService;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@Api(value="Webhook", description="Operations pertaining to registering Webhooks to listend for EVents from JSD Server")
@RequestMapping(value="/webhooks")
public class WebhookController {
	
	@Autowired
	EntityService service;
	
	@Autowired
	EntityListService listService;
		
	@IgnoreJwt
	@ApiOperation(value = "Retrieve a list of current Webhooks for a specific client")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody ResponseWrapperDTO retrieveAll(
    		@ApiParam(name="hostUser", value="The HostUser with the current Client Key for this request", required=false) @AuthenticationPrincipal AtlassianHostUser hostUser) throws Exception {
		ResponseWrapperDTO retval = listService.retrieveAllWebHooksByClientId(prepWebHook(new WebHook(), hostUser));
        return retval;
    }
	
	@IgnoreJwt
	@ApiOperation(value = "Retrieve a Specific Webhook by Webhooks Id")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseWrapperDTO retrieve(
    		@ApiParam(name="hostUser", value="The HostUser with the current Client Key for this request", required=false) @AuthenticationPrincipal AtlassianHostUser hostUser, 
    		@ApiParam(name="id", value="The Id of the Webhook to retrieve", required=true) @PathVariable int id) throws Exception {
		ResponseWrapperDTO retval = service.retrieve(prepWebHook(new WebHook(id), hostUser), WebHook.class);
        return retval;
    }	
	
	@IgnoreJwt
	@ApiOperation(value = "Create a new Webhook for a ProjectRegistration")
    @RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseStatus( HttpStatus.CREATED )
    public @ResponseBody ResponseWrapperDTO create(
    		@ApiParam(name="hostUser", value="The HostUser with the current Client Key for this request", required=false) @AuthenticationPrincipal AtlassianHostUser hostUser, 
    		@ApiParam(name="registration", value="The Webhook object to Create", required=true) @RequestBody WebHook webhook) throws Exception {
		ResponseWrapperDTO retval = service.create(prepWebHook(webhook, hostUser), WebHook.class);
        return retval;
    }
	
	@IgnoreJwt
	@ApiOperation(value = "Update an existing Webhook by Id")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public @ResponseBody ResponseWrapperDTO update(
    		@ApiParam(name="hostUser", value="The HostUser with the current Client Key for this request", required=false) @AuthenticationPrincipal AtlassianHostUser hostUser, 
    		@ApiParam(name="registration", value="The Webhook object to Update", required=true) @RequestBody WebHook webhook, 
    		@ApiParam(name="id", value="The Id of the Webhook to update", required=true) @PathVariable int id) throws Exception  {
		webhook.setId(id);
		ResponseWrapperDTO retval = service.update(prepWebHook(webhook, hostUser), WebHook.class);
        return retval;
    }
	
	@IgnoreJwt
	@ApiOperation(value = "Delete an existing Webhook by Id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseWrapperDTO delete(
    		@ApiParam(name="hostUser", value="The HostUser with the current Client Key for this request", required=false) @AuthenticationPrincipal AtlassianHostUser hostUser, 
    		@ApiParam(name="id", value="The Id of the Webhook to delete", required=true) @PathVariable int id) throws Exception  {
		ResponseWrapperDTO retval = service.delete(prepWebHook(new WebHook(id), hostUser), WebHook.class);
        return retval;
    }
	
	private WebHook prepWebHook(WebHook webhook, AtlassianHostUser hostUser){
		if(hostUser!=null && hostUser.getHost()==null){
			webhook.setClientKey(hostUser.getHost().getClientKey());
		}
		return webhook;
	}
}
