package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.connect.spring.AtlassianHost;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.dao.EntityListDao;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Project;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.ProjectRegistration;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Splunk;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.WebHook;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;


/**
 * The JSDService class.
 *
 * @param <T> the generic type
 */
@Component
public class EntityListService {

	@Autowired
	private EntityListDao dao;
	
	/* Method used to retrieve All ProjectRegistrations. 
	 * 
	 * Entity found will be placed in the payload field of the ResponseWrapper.
	 */
	public ResponseWrapperDTO retrieveAllProjectRegistrationsByClientId(ProjectRegistration registration) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		Collection<ProjectRegistration> retval = dao.getAllProjectRegistrationsByClientId(registration);
		
		if (retval == null || retval.isEmpty()) {
			response.setPayload(null);
			response.setResponseCode(404);
			response.setMessage(ProjectRegistration.class.getSimpleName() + " not fount");
			response.setLocation("/" + ProjectRegistration.class.getSimpleName() + "/");
		} else {
			response.setPayload(retval);
			response.setResponseCode(200);
			response.setMessage(ProjectRegistration.class.getSimpleName() + " Successfully Found:" + registration.getId());
			response.setLocation("/" + ProjectRegistration.class.getSimpleName() + "/");
		}
		
		return response;
	}
	
	/* Method used to retrieve All WebHooks. 
	 * 
	 * Entity found will be placed in the payload field of the ResponseWrapper.
	 */
	public ResponseWrapperDTO retrieveAllWebHooksByClientId(WebHook webhook) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		Collection<WebHook> retval = dao.getAllWebHooksByClientId(webhook);
		
		if (retval == null || retval.isEmpty()) {
			response.setPayload(null);
			response.setResponseCode(404);
			response.setMessage(WebHook.class.getSimpleName() + " not fount");
			response.setLocation("/" + WebHook.class.getSimpleName() + "/");
		} else {
			response.setPayload(retval);
			response.setResponseCode(200);
			response.setMessage(WebHook.class.getSimpleName() + " Successfully Found:" + webhook.getId());
			response.setLocation("/" + WebHook.class.getSimpleName() + "/");
		}
		
		return response;
	}

	public ResponseWrapperDTO retrieveAllProjectsByClientId(Project project) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		Collection<Project> retval = dao.getAllProjectsByClientId(project);
		
		if (retval == null || retval.isEmpty()) {
			response.setPayload(null);
			response.setResponseCode(404);
			response.setMessage(Project.class.getSimpleName() + " not fount");
			response.setLocation("/" + Project.class.getSimpleName() + "/");
		} else {
			response.setPayload(retval);
			response.setResponseCode(200);
			response.setMessage(Project.class.getSimpleName() + " Successfully Found:" + project.getId());
			response.setLocation("/" + Project.class.getSimpleName() + "/");
		}
		
		return response;
	}

	public ResponseWrapperDTO retrieveAllSplunksByClientId(Splunk splunk) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		Collection<Project> retval = dao.getAllSplunksByClientId(splunk);
		
		if (retval == null || retval.isEmpty()) {
			response.setPayload(null);
			response.setResponseCode(404);
			response.setMessage(Project.class.getSimpleName() + " not fount");
			response.setLocation("/" + Project.class.getSimpleName() + "/");
		} else {
			response.setPayload(retval);
			response.setResponseCode(200);
			response.setMessage(Project.class.getSimpleName() + " Successfully Found:" + splunk.getId());
			response.setLocation("/" + Project.class.getSimpleName() + "/");
		}
		
		return response;
	}
	
	/* Method used to retrieve All ProjectRegistrations. 
	 * 
	 * Entity found will be placed in the payload field of the ResponseWrapper.
	 */
	public ResponseWrapperDTO retrieveAllHostUsersByClientId(String clientKey) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		AtlassianHost retval = dao.getAllHostUsersByClientId(clientKey);
		
		if (retval == null) {
			response.setPayload(null);
			response.setResponseCode(404);
			response.setMessage(ProjectRegistration.class.getSimpleName() + " not fount");
			response.setLocation("N/A");
		} else {
			response.setPayload(retval);
			response.setResponseCode(200);
			response.setMessage(ProjectRegistration.class.getSimpleName() + " Successfully Found:" + clientKey);
			response.setLocation("N/A");
		}
		
		return response;
	}
}
