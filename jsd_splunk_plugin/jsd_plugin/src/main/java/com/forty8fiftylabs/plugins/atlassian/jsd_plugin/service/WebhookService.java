package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.dao.EntityDao;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.ProjectRegistration;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.WebHook;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.Model;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;


/**
 * The JSDService class.
 *
 * @param <T> the generic type
 */
@Component
public class WebhookService {

	@Autowired
	private EntityDao dao;
	
	/* Method used to create an Entity. 
	 * 
	 * Entity created will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO create(T m, Class<?> clazz) throws Exception {
		ResponseWrapperDTO response = new ResponseWrapperDTO();

		try {
			response.setPayload(dao.addEntity(m));
			response.setResponseCode(201);
			response.setMessage(clazz.getSimpleName() + " Successfully Created:" + m.getId());
			response.setLocation("/" + clazz.getSimpleName() + "/" + m.getId());
		} catch (Exception e) {
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + " Creation Failed:" + m.getId());
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}

	/* Method used to update an Entity. 
	 * 
	 * Entity updated will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO update(T m, Class<?> clazz) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		try {			
			response.setPayload(dao.updateEntity(m));
			response.setResponseCode(200);
			response.setMessage(clazz.getSimpleName() + " Successfully Updated:" + m.getId());
			response.setLocation("/" + clazz.getSimpleName() + "/" + m.getId());
		} catch (Exception e) {
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + " Update Failed:" + m.getId());
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}

	/* Method used to delete an Entity. 
	 * 
	 * Null will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO delete(T m, Class<?> clazz) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();

		int id = m.getId();

		try {
			Model retval = (Model) dao.getEntityById(clazz, id);

			if (retval == null) {
				response.setPayload(null);
				response.setResponseCode(404);
				response.setMessage(clazz.getSimpleName() + " not fount to Delete:" + id);
				response.setLocation("/" + clazz.getSimpleName() + "/" + id);
			} else {
				dao.deleteEntity(retval);
				response.setPayload(null);
				response.setResponseCode(204);
				response.setMessage(clazz.getSimpleName() + " Successfully Deleted:" + id);
			}
		} catch (Exception e) {
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + " Delete Failed:" + id);
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}

	/* Method used to retrieve an Entity by ID. 
	 * 
	 * Entity found will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO retrieve(T m, Class<?> clazz) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();

		int id = m.getId();

		try {
			Model retval = (Model) dao.getEntityById(clazz, id);

			if (retval == null) {
				response.setPayload(null);
				response.setResponseCode(404);
				response.setMessage(clazz.getSimpleName() + " not found:" + id);
				response.setLocation("/" + clazz.getSimpleName() + "/" + id);
			} else {
				response.setPayload(retval);
				response.setResponseCode(200);
				response.setMessage(clazz.getSimpleName() + " Successfully Found:" + m.getId());
				response.setLocation("/" + clazz.getSimpleName() + "/" + id);
			}
		} catch (Exception e) {
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + " Search Failed:" + id);
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}

	/* Method used to find a list of Entities by matching a value in a specific field. 
	 * 
	 * An array of Entities matched will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO retrieveBy(String field, String value, T m, Class<?> clazz) {
		ResponseWrapperDTO response = new ResponseWrapperDTO();

		try {
			List<?> l = dao.retrieveBy(field, value, clazz);

			if (l == null || l.isEmpty()) {
				response.setPayload(null);
				response.setResponseCode(404);
				response.setMessage(clazz.getSimpleName() + "s not found:" + field + "=" + value);
			} else {
				response.setPayload(l);
				response.setResponseCode(200);
				response.setMessage(
						clazz.getSimpleName() + "s Successfully Found:" + field + "=" + value);
			}
		} catch (Exception e) {
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + "s Retrieval Failed:" + field + "=" + value);
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}

	/* Method used to find a list of Child Entities belonging to a specific Entity. 
	 * 
	 * An array of Child Entities found will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO retrieveList(String id, String field, T m, Class<?> clazz) throws Exception {
		ResponseWrapperDTO response = new ResponseWrapperDTO();

		try {
			Set<?> l = dao.retrieveAttributeList(id, field, clazz);

			if (l == null || l.isEmpty()) {
				response.setPayload(null);
				response.setResponseCode(404);
				response.setMessage(clazz.getSimpleName() + "s Child not found for Entity id:" + id
						+ " attribute:" + field);
			} else {
				response.setPayload(l);
				response.setResponseCode(200);
				response.setMessage(clazz.getSimpleName() + "s Child found for Entity id:" + id
						+ " attribute: " + field);
			}
		} catch (Exception e) {
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + "s Child Search Failed for id:" + id
					+ " attribute: " + field);
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}

	/* Method used to find a list of Entities by matching a key-value pairs from a query string.
	 * The key should match a field name, and the value is what is searched for in the field.
	 * 
	 * An array of Entities matched will be placed in the payload field of the ResponseWrapper.
	 */
	public <T extends Model> ResponseWrapperDTO retrieveQuery(String qs, T m, Class<?> clazz) throws Exception {
		ResponseWrapperDTO response = new ResponseWrapperDTO();
		
		try{
			if(qs==null || qs.equals("")) throw new Exception("No parameters were found in the request.");
			
			List<NameValuePair> params = URLEncodedUtils.parse(new URI("?" + qs), "UTF-8");
			
			if(params==null || params.isEmpty())throw new Exception("Parameters could not be the parsed from the request.");
		
			List<?> l = dao.retrieveBy(params, clazz);
			
			if (l == null || l.isEmpty()) {
				response.setPayload(null);
				response.setResponseCode(404);
				response.setMessage(clazz.getSimpleName() + " not found for query:" + qs);
			} else {
				response.setPayload(l);
				response.setResponseCode(200);
				response.setMessage(clazz.getSimpleName() + " found for query:" + qs);
			}
		} catch (Exception e){
			response.setPayload(null);
			response.setResponseCode(500);
			response.setMessage(clazz.getSimpleName() + "s Search Failed for query:" + qs);
			response.setMessage(e.getLocalizedMessage());
			response.setException(ExceptionUtils.getRootCause(e));
		}
		
		return response;
	}
	
	public WebHook createDefault(ProjectRegistration registration){		
		WebHook issuesWebHook = new WebHook();
		issuesWebHook.setName("WH for " + registration.getProject());
		issuesWebHook.setClientKey(registration.getClientKey());
		List<String> events = new ArrayList<String>();
		events.add("jira:issue_created");
		events.add("jira:issue_updated");
		issuesWebHook.setEvents(events);
		return issuesWebHook;		
	}
}
