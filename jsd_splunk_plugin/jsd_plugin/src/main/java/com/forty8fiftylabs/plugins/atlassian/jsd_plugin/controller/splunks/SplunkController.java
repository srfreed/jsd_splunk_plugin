package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.controller.splunks;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.connect.spring.IgnoreJwt;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Splunk;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityListService;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@Api(value="Splunk", description="Operations pertaining to Splunks")
@RequestMapping(value="/splunks")
public class SplunkController {
	
	@Autowired
	EntityService service;
	
	@Autowired
	EntityListService listService;

	@IgnoreJwt
	@ApiOperation(value = "Retrieve a list of current Splunks for a specific client")
    @RequestMapping(value = "/clientKey/{clientKey}", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody ResponseWrapperDTO retrieveAll(
    		@ApiParam(name="clientKey", value="The clientKey for the Request", required=true) @PathVariable String clientKey, HttpServletResponse response) throws Exception {
		ResponseWrapperDTO retval = listService.retrieveAllSplunksByClientId(prepSplunk(new Splunk(), clientKey));
		
		
		response.setStatus(HttpServletResponse.SC_OK);

		return retval;
    }
	
	@IgnoreJwt
	@ApiOperation(value = "Retrieve a Specific Splunk by Splunk Id")
    @RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseWrapperDTO retrieve(
    		@ApiParam(name="clientKey", value="The clientKey for the Request", required=true) @PathVariable String clientKey,
    		@ApiParam(name="id", value="The Id of the Splunk to retrieve", required=true) @PathVariable int id, HttpServletResponse response) throws Exception {
		ResponseWrapperDTO retval = service.retrieve(prepSplunk(new Splunk(id), clientKey), Splunk.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
        return retval;
    }	
	
	@IgnoreJwt
	@ApiOperation(value = "Create a new Splunk for a Client")
	@RequestMapping(value = "/clientKey/{clientKey}", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody ResponseWrapperDTO create(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "splunk", value = "The Splunk object to Create", required = true) @RequestBody Splunk splunk, HttpServletResponse response)
			throws Exception {

		ResponseWrapperDTO retval = service.create(prepSplunk(splunk, clientKey),
				Splunk.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	@IgnoreJwt
	@ApiOperation(value = "Update an existing Splunk by Id")
	@RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseWrapperDTO update(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "splunk", value = "The Splunk object to Update", required = true) @RequestBody Splunk splunk,
			@ApiParam(name = "id", value = "The Id of the Splunk to update", required = true) @PathVariable int id, HttpServletResponse response)
			throws Exception {
		splunk.setId(id);
		
		ResponseWrapperDTO retval = service.update(prepSplunk(splunk, clientKey),
				Splunk.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	@IgnoreJwt
	@ApiOperation(value = "Delete and existing Splunk by Id")
	@RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseWrapperDTO delete(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "id", value = "The Id of the Splunk to delete", required = true) @PathVariable int id, HttpServletResponse response)
			throws Exception {
		ResponseWrapperDTO retval = service.delete(prepSplunk(new Splunk(id), clientKey),
				Splunk.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	
	private Splunk prepSplunk(Splunk splunk, String clientKey){
		if(clientKey!=null && !clientKey.equals("")){
			splunk.setClientKey(clientKey);
		}
		return splunk;
	}
}
