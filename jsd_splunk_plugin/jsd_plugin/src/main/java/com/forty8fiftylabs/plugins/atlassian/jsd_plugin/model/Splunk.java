package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.Model;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.ModelBase;

@Entity
@Table(name="Splunks")
public class Splunk extends ModelBase implements Model, Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SPLUNK_ID")
	private int id;
	
	@Column(name = "SPLUNK_NAME")
	private String name;
	
	@Column(name = "SPLUNK_URL")
	private String url;
	
	@Column(name = "SPLUNK_CLIENT_KEY")
	private String clientKey;
	
	@OneToOne(mappedBy = "project", optional = false, fetch=FetchType.EAGER)
	private ProjectRegistration registration;
	
	public Splunk() {}
	
	public Splunk(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getClientKey() {
		return clientKey;
	}
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public ProjectRegistration getRegistration() {
		return registration;
	}

	public void setRegistration(ProjectRegistration registration) {
		this.registration = registration;
	}
}
