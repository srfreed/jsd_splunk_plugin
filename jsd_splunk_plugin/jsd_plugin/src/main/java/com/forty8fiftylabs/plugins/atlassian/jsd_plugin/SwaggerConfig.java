package com.forty8fiftylabs.plugins.atlassian.jsd_plugin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.paths.SwaggerPathProvider;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import com.wordnik.swagger.model.ApiInfo;


@Configuration
@EnableSwagger
public class SwaggerConfig {
	private SpringSwaggerConfig springSwaggerConfig;

	@Autowired
	public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
		this.springSwaggerConfig = springSwaggerConfig;
	}

	@Bean
	public SwaggerSpringMvcPlugin customImplementation() {
		return new SwaggerSpringMvcPlugin(this.springSwaggerConfig).apiInfo(apiInfo()).includePatterns("/.*").pathProvider(new SwaggerPathProvider() {

            protected String getDocumentationPath() {
                // TODO Auto-generated method stub
                return "/";
            }

            protected String applicationPath() {
                // TODO Auto-generated method stub
                return "../";
            }
        });
	}

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo("JSD Plugin API", "API for JSD Plugin", "Forty8Fifty JSD Plugin API terms of service",
				"sfreed@veristor.com", " ", " ");
		return apiInfo;
	}
}
