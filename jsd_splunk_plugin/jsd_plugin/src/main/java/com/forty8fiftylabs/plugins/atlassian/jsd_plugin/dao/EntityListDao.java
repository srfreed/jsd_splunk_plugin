package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.atlassian.connect.spring.AtlassianHost;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Project;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.ProjectRegistration;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Splunk;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.WebHook;

/**
 * Class used accessing the CHUB Database.
 * Each method leverages generic types and uses the Entity Manager 
 * defined in the Spring Context.
 * 
 */
@Component
public class EntityListDao {
	
	private static final Logger logger = Logger.getLogger(EntityListDao.class);

	@PersistenceContext
	private EntityManager em;
	
	/**
	 * Get an list of ProjectRegistration entities by client key.
	 *
	 * @param <T> 			the ProjectRegistration class containing the Client Key
	 * @return 				The entity, or <code>null</code> if none exists.
	 * @throws 				Exception the exception thrown from the find call
	 */
	@SuppressWarnings("unchecked")
	public Collection<ProjectRegistration> getAllProjectRegistrationsByClientId(ProjectRegistration registration) {
		Collection<ProjectRegistration> entities = null;
		try {
			Query query = em.createQuery("SELECT e FROM ProjectRegistration e WHERE e.clientKey = :clientKey");
			query.setParameter("clientKey", registration.getClientKey());
			entities = query.getResultList();
		} catch (Exception e) {
			logger.error("Couldn't find all " + ProjectRegistration.class.getName() , e);
			throw e;
		}

		return entities;
	}	
	
	/**
	 * Get an list of WebHook entities by client key.
	 *
	 * @param <T> 			the WebHook class containing the Client Key
	 * @return 				The entity, or <code>null</code> if none exists.
	 * @throws 				Exception the exception thrown from the find call
	 */
	@SuppressWarnings("unchecked")
	public Collection<WebHook> getAllWebHooksByClientId(WebHook webhook) {
		Collection<WebHook> entities = null;
		try {
			Query query = em.createQuery("SELECT e FROM WebHook e WHERE e.clientKey = :clientKey");
			query.setParameter("clientKey", webhook.getClientKey());
			entities = query.getResultList();
		} catch (Exception e) {
			logger.error("Couldn't find all " + WebHook.class.getName() , e);
			throw e;
		}

		return entities;
	}	
	
	/**
	 * Get an list of Project entities by client key.
	 *
	 * @param <T> 			the Project class containing the Client Key
	 * @return 				The entity, or <code>null</code> if none exists.
	 * @throws 				Exception the exception thrown from the find call
	 */
	@SuppressWarnings("unchecked")
	public Collection<Project> getAllProjectsByClientId(Project project) {
		Collection<Project> entities = null;
		try {
			Query query = em.createQuery("SELECT e FROM Project e WHERE e.clientKey = :clientKey");
			query.setParameter("clientKey", project.getClientKey());
			entities = query.getResultList();
		} catch (Exception e) {
			logger.error("Couldn't find all " + Project.class.getName() , e);
			throw e;
		}

		return entities;
	}

	@SuppressWarnings("unchecked")
	public Collection<Project> getAllSplunksByClientId(Splunk splunk) {
		Collection<Project> entities = null;
		try {
			Query query = em.createQuery("SELECT e FROM Splunk e WHERE e.clientKey = :clientKey");
			query.setParameter("clientKey", splunk.getClientKey());
			entities = query.getResultList();
		} catch (Exception e) {
			logger.error("Couldn't find all " + Project.class.getName() , e);
			throw e;
		}

		return entities;
	}	
	
	@SuppressWarnings("unchecked")
	public AtlassianHost getAllHostUsersByClientId(String clientKey) {
		List<AtlassianHost> entities = null;
		try {
			Query query = em.createNativeQuery("SELECT * FROM atlassian_host WHERE client_key = ?", AtlassianHost.class);
			query.setParameter(1, clientKey);
			entities = query.getResultList();
		} catch (Exception e) {
			logger.error("Couldn't find " + AtlassianHost.class.getName() + " with clientKey: " + clientKey  , e);
			throw e;
		}

		return entities.get(0);
	}		

}
