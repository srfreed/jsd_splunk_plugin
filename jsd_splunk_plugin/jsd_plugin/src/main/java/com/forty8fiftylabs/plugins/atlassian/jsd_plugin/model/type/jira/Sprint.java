package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.type.jira;

public enum Sprint {
	sprint_created,
	sprint_deleted,
	sprint_updated,
	sprint_started,
	sprint_closed
}
