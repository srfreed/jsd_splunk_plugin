package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.controller.projects;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.IgnoreJwt;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Project;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.ProjectRegistration;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.Splunk;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.WebHook;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.dto.ResponseWrapperDTO;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityListService;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.EntityService;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.service.WebhookService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@Api(value = "ProjectRegistration", description = "Operations pertaining to Registering JSD Projects for Splunk Intgegration")
@RequestMapping(value = "/projectRegistration")
public class ProjectRegistrationController {

	private static String serviceDeskURI = "/rest/webhooks/1.0/webhook";
	
	@Autowired
	EntityService service;

	@Autowired
	EntityListService listService;
	
	@Autowired
	WebhookService webhookService;
	
	@Autowired
	private RestTemplate restTemplate;
	
//	@Autowired
//	private JwtGenerator jwtGen;
//	
    @Autowired
    private AtlassianHostRepository hostRepository;

	@IgnoreJwt
	@ApiOperation(value = "Retrieve a list of current Registrations for a specific client")
	@RequestMapping(value = "/clientKey/{clientKey}", method = RequestMethod.GET)
	public @ResponseBody ResponseWrapperDTO retrieveAll(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey, HttpServletResponse response)
			throws Exception {
		ResponseWrapperDTO retval = listService.retrieveAllProjectRegistrationsByClientId(
				prepProjectRegistration(new ProjectRegistration(), clientKey));
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	@IgnoreJwt
	@ApiOperation(value = "Retrieve a Specific Registration  by Registration Id")
	@RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseWrapperDTO retrieve(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "id", value = "The Id of the Registration to retrieve", required = true) @PathVariable int id, HttpServletResponse response)
			throws Exception {
		ResponseWrapperDTO retval = service.retrieve(prepProjectRegistration(new ProjectRegistration(id), clientKey),
				ProjectRegistration.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	@IgnoreJwt
	@ApiOperation(value = "Create a new Registration for a Client")
	@RequestMapping(value = "/clientKey/{clientKey}", method = RequestMethod.POST)
	public @ResponseBody ResponseWrapperDTO create(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "registration", value = "The Registration object to Create", required = true) @RequestBody ProjectRegistration registration, HttpServletResponse response)
			throws Exception {
		
		Project project = (Project) service.retrieve(registration.getProject(), Project.class).getPayload();
		Splunk splunk = (Splunk) service.retrieve(registration.getSplunk(), Splunk.class).getPayload();
		
		registration.setProject(project);
		registration.setSplunk(splunk);

		//https://jsdplugin1.veristor.com/webhooks/message
		
		String baseURL = "https://forty8fiftylabs.atlassian.net";
		String contentUrl = String.format("%s" + serviceDeskURI, baseURL);
		
		//hostUser = hostRepository.findFirstByBaseUrl(baseURL).get();//(AtlassianHost)listService.retrieveAllHostUsersByClientId(clientKey).getPayload();
//		//AtlassianHostUser hostUser = new AtlassianHostUser(host, Optional.of("sfreed")); 
//		
//		String token = jwtGen.createJwtToken(new URI(baseURL), HttpMethod.POST, hostUser);
//		
//		System.out.println(token);
		WebHook webhook = webhookService.createDefault(registration);
		
		ResponseEntity<String> webhookReturn = restTemplate.postForEntity(contentUrl, webhook, String.class);
		System.out.println("Webhook Registered:" + webhookReturn.getBody());
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		ResponseWrapperDTO retval = service.create(prepProjectRegistration(registration, clientKey),
				ProjectRegistration.class);
		return retval;
	}

	@IgnoreJwt
	@ApiOperation(value = "Update an existing Registration by Id")
	@RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseWrapperDTO update(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "registration", value = "The Registration object to Update", required = true) @RequestBody ProjectRegistration registration,
			@ApiParam(name = "id", value = "The Id of the Registration to update", required = true) @PathVariable int id, HttpServletResponse response)
			throws Exception {
		registration.setId(id);
		
		Project project = (Project) service.retrieve(registration.getProject(), Project.class).getPayload();
		Splunk splunk = (Splunk) service.retrieve(registration.getSplunk(), Splunk.class).getPayload();
		
		registration.setProject(project);
		registration.setSplunk(splunk);
		
		ResponseWrapperDTO retval = service.update(prepProjectRegistration(registration, clientKey),
				ProjectRegistration.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	@IgnoreJwt
	@ApiOperation(value = "Delete and existing Registration by Id")
	@RequestMapping(value = "/clientKey/{clientKey}/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseWrapperDTO delete(
			@ApiParam(name = "clientKey", value = "The clientKey for the Request", required = true) @PathVariable String clientKey,
			@ApiParam(name = "id", value = "The Id of the Registration to delete", required = true) @PathVariable int id, HttpServletResponse response)
			throws Exception {
		ResponseWrapperDTO retval = service.delete(prepProjectRegistration(new ProjectRegistration(id), clientKey),
				ProjectRegistration.class);
		
		response.setStatus(HttpServletResponse.SC_OK);
		
		return retval;
	}

	private ProjectRegistration prepProjectRegistration(ProjectRegistration project, String clientKey) {
		if (clientKey != null && !clientKey.equals("")) {
			project.setClientKey(clientKey);
		}
		return project;
	}
}
