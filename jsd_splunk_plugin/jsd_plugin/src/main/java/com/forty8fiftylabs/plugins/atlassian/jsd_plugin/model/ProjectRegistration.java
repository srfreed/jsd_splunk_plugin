package com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.Model;
import com.forty8fiftylabs.plugins.atlassian.jsd_plugin.model.base.ModelBase;

@Entity
@Table(name="ProjectRegistration")
public class ProjectRegistration extends ModelBase implements Model, Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PR_ID")
	private int id;
	
	@OneToOne(optional = false, fetch=FetchType.EAGER)
	@JoinColumn(name="SPLUNK_ID")
	private Splunk splunk;
	
	@Column(name = "PR_CLIENT_KEY")
	private String clientKey;
	
	@OneToOne(optional = false, fetch=FetchType.EAGER)
	@JoinColumn(name="PROJECT_ID")
	private Project project;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name = "PR_ID")
	private Set<WebHook> webhooks = new HashSet<WebHook>();
	
	public ProjectRegistration(){}
	
	public ProjectRegistration(int id) {
		this.id = id;
	}
	
	@Override
	public int getId() {
		return id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
	}
	public Splunk getSplunk() {
		return splunk;
	}

	public void setSplunk(Splunk splunk) {
		this.splunk = splunk;
	}

	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public String getClientKey() {
		return clientKey;
	}
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}
	public Set<WebHook> getWebhooks() {
		return webhooks;
	}
	public void setWebhooks(Set<WebHook> webhooks) {
		this.webhooks = webhooks;
	}
}
