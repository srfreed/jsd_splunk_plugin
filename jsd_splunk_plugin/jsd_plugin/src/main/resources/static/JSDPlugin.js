    var WebhookModel = function (webhooks) {
        var self = this;
        self.webhooks = ko.observableArray(webhooks);

        self.addWebhook = function () {
            self.webhooks.push({
                /*
                     Project names are hardcoded for now.
                     Will be replaced with the user's project names
                     by using AJAX for KnockOut.
                     http://knockoutjs.com/documentation/json-data.html
                 */
                project: ko.observableArray(['Project1', 'Project2']),
                splunkURL: ""
            });
        };

        self.removeWebhook = function (webhook) {
            alert("Are you sure you want to remove this webhook?")
            self.webhooks.remove(webhook);
        };

        self.save = function (form) {
            alert("Could now transmit to server: " + ko.utils.stringifyJson(self.webhooks));
            // To actually transmit to server as a regular form post, write this: ko.utils.postJson($("form")[0], self.webhooks);
        };
    };

    var viewModel = new WebhookModel([
        /*
             Project names are hardcoded for now.
             Will be replaced with the user's project names
             by using AJAX for KnockOut.
             http://knockoutjs.com/documentation/json-data.html
         */
        {project: ko.observableArray(['Project1', 'Project2']), splunkURL: ""}

    ]);
    ko.applyBindings(viewModel);

    // Activate jQuery Validation
    $("form").validate({submitHandler: viewModel.save});