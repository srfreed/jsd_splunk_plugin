define(['plugins/router', 'durandal/app', 'knockout', 'registration', 'webhook', 'project', 'splunk'], function(router, app, ko, registration, webhook, project, splunk) {
	return {
		displayName : ko.observable(),
		splunk : ko.observable(),
		webhook : ko.observable(),
		
		activate : function(id) {
			var self = this;
			if(id){
				self.splunk(splunk.load(id, function(){
					self.displayName('Edit Splunk Server');
				}));
			} else {
				self.splunk(splunk.reset(function(){
					self.displayName('Add Splunk Server');
				}));	
			}
		},
		saveSplunk : function(data) {
			var self = this;

			//save the splunk and navigate back to splunks list.
			splunk.save(data, function(){
				router.navigate('splunks');	
			});
			
		}, 
		cancel: function(data){
			router.navigate('splunks');
		}, 
		deactivate : function (data){
			//called when the form changes.
		}
	};
});