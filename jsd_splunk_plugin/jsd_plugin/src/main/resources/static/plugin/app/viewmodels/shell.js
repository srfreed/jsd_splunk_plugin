define(['plugins/router', 'durandal/app'], function (router, app) {
    return {
        router: router,
        search: function() {
            //It's really easy to show a message box.
            //You can add custom options too. Also, it returns a promise for the user's response.
            app.showMessage('Search not yet implemented...');
        },
        activate: function () {
            router.map([
                { route:'', title: 'Registrations', moduleId: 'viewmodels/registrations', nav: true },
                { route:'registrations', title: 'Registrations', moduleId: 'viewmodels/registrations'},
                { route:'registration(/:registrationsId)', title: 'Registration', moduleId: 'viewmodels/registration' },
                { route:'registrations/:registrationsId/webhook(/:webhookId)', title: 'Webhook', moduleId: 'viewmodels/webhook'},
                
                { route:'splunks', title: 'Splunks Servers', moduleId: 'viewmodels/splunks', nav: true, },
                { route:'splunk(/:id)', title: 'Splunk Server', moduleId: 'viewmodels/splunk' }
            ]).buildNavigationModel();
            
            router.isNavigating=false;
            return router.activate();
        }
    };
});