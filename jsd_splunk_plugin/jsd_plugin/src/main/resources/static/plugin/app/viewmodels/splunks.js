define(['plugins/router', 'durandal/app', 'knockout', 'registration', 'webhook', 'project', 'splunk'], function(router, app, ko, registration, webhook, project, splunk) {
	return {
		displayName : 'Splunk Servers',
		splunks : ko.observableArray([]),

		activate : function() {
			this.loadSplunks(this);
		},
		loadSplunks: function(data){
			splunk.findAll(data, function(){});
		},
		addSplunk : function(data) {
			router.navigate('splunk');
		},
		editSplunk : function(data, item) {
			router.navigate('splunk/'+item.id);
		}, 
		deleteSplunk: function(data, item){
			splunk.remove(item.id, function(){
				data.splunks.remove(item);
			});
		},
		cancel: function(data){
			router.navigate('splunks');
		}, 
		deactivate : function (data){
			//called when the form changes.
		}
	};
});