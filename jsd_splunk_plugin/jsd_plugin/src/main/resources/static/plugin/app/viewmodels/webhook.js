define(['plugins/router', 'durandal/app', 'knockout', 'registration', 'webhook', 'project', 'splunk'], function(router, app, ko, registration, webhook, project, splunk) {
	return {
		displayName : ko.observable(),
		webhook : ko.observable(),
		registration : ko.observable(),
		allWebhooks: ko.observableArray([]),

		activate : function(registrationId, webhookId) {
			var self = this;
			
			self.registration(registration.load(registrationId));
			
			if(webhookId){
				//get the webhook from the registration
				for(id in self.registration().webhooks()){
					var p = self.registration().webhooks()[id];
					if(p.id==id){
						var newP = webhook.reset();						
						newP.id(p.id);
						newP.command(p.command);
						self.webhook(newP);
					}	
				}
				
				self.displayName('Edit Parent');				
			} else {
				self.webhook(webhook.reset(function(){
					self.displayName('Add Parent');					
				}));
			}			
			
			self.loadWebhooks(this);
		},
		loadWebhooks: function(data){
			webhook.findAll(data);
		},
		saveWebhook: function(data){		
			delete data.webhook().__moduleId__;
			
			var isNew = true;
			
			//set the webhook in the Registration
			for(id in data.registration().webhooks()){
				var p = data.registration().webhooks()[id];
				if(p.id==data.webhook().id()){
					data.registration().webhooks()[id] = data.webhook();
					isNew = false;
				}	
			}
			
			//if the Webhook is new, push the parent onto the Registration
			if(isNew){
				data.registration().webhooks().push(data.webhook());
			}
			
			registration.save(data, function(){
				router.navigate('registration/'+data.registration().id());
			});			
		},
		setWebhook: function(data){	
			if(data.selectedWebhook()){
				data.webhook().webhookId(data.selectedWebhook().id);
				data.webhook().command(data.selectedWebhook().command);
			}
		},
		cancel: function(data){
			data.webhook(webhook.reset());
			router.navigate('registration/'+data.registration().id());
		}, 
		deactivate : function (data){
			//called when the form changes.
		}
	};
});