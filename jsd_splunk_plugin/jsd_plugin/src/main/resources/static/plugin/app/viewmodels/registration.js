define(['plugins/router', 'durandal/app', 'knockout', 'registration', 'webhook', 'project', 'splunk'], function(router, app, ko, registration, webhook, project, splunk ) {
	return {
		displayName : ko.observable(),
		registration : ko.observable(),
		webhook : ko.observable(),
		projects : ko.observableArray([]),
		project : ko.observable(),
		
		activate : function(id) {
			var self = this;
			if(id){
				self.registration(registration.load(id, function(){
					self.displayName('Edit Registration');
				}));
			} else {
				self.registration(registration.reset(function(){
					self.displayName('Add Registration');
				}));	
			}
		},
		saveRegistration : function(data) {
			var self = this;
			var p = {
				id: self.registration().projectId()
			};
			self.registration().project(p);
			
			var s = {
					id: self.registration().splunkId()
				};
			self.registration().splunk(s);

			//save the registration and navigate back to registrations list.
			registration.save(data, function(){
				router.navigate('registrations');	
			});
		}, 
		addWebhook: function(data){	
			router.navigate('registration/'+data.registration().id()+'/webhook');
		},		
		editWebhook : function(data, item) {
			router.navigate('registration/'+data.registration().id()+'/webhook/'+item.id);
		},		
		removeWebhook: function(data, item){	
			//remove the webhook from the registration
			for(id in data.registration().webhooks()){
				var p = data.registration().webhooks()[id];
				if(p.id==item.id){
					data.registration().webhooks.remove(p);
				}	
			}
			
			//save registration
			registration.save(data, function(){
				router.navigate('registration/'+data.registration().registrationId());
			});	
		},
		cancel: function(data){
			router.navigate('registrations');
		}, 
		deactivate : function (data){
			//called when the form changes.
		}
	};
});