define(['plugins/router', 'durandal/app', 'knockout', 'registration', 'webhook', 'project', 'splunk'], function(router, app, ko, registration, webhook, project, splunk) {
	return {
		displayName : 'Registrations',
		registrations : ko.observableArray([]),

		activate : function() {
			this.loadRegistrations(this);
		},
		loadRegistrations: function(data){
			registration.findAll(data, function(){});
		},
		addRegistration : function(data) {
			router.navigate('registration');
		},
		editRegistration : function(data, item) {
			router.navigate('registration/'+item.id);
		}, 
		deleteRegistration: function(data, item){
			registration.remove(item.id, function(){
				data.registrations.remove(item);
			});
		},
		cancel: function(data){
			router.navigate('registrations');
		}, 
		deactivate : function (data){
			//called when the form changes.
		}
	};
});