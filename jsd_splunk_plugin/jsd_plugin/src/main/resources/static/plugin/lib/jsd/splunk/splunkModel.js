define(['knockout', 'webhook', 'project'], function(ko) {

	var SERVER_URL = '/splunks';

	return {
		id: ko.observable(),
		name : ko.observable(),
		url : ko.observable(),
		
		reset: function(callback){
			var self = this;
			self.id(0);
			self.name('');
			self.url('');

			
			if(callback)callback();
			
			return self;
		},
		load: function(id, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey + '/' + id,
				success : function(data) {
					self.id(data.payload.id);
					self.name(data.payload.name);
					self.url(data.payload.url);
					
					if(callback)callback();
				},
				dataType : 'json',
				contentType : 'application/json'
			});
			return self;
		},
		remove: function(id, callback){
			$.ajax({
				type : "DELETE",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey + '/' + id,
				success : function(data) {
					if(callback)callback();
				},				
				dataType : 'json',
				contentType : 'application/json'
			});
			return true;
		},
		findAll: function(view, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey,
				success : function(data) {
					view.splunks.removeAll();
					ko.utils.arrayMap(data.payload, function(p) {
						view.splunks.push(p);
					});
					splunkList = view.splunks();
					if(callback)callback();
				},
				dataType : 'json',
				contentType : 'application/json'
			});
		},
		save: function(view, callback){
			delete view.splunk().__moduleId__;
			
			var URI = SERVER_URL + '/clientKey/' +clientKey;
			if(view.splunk().id()!='' && view.splunk().id()!='0'){
				
				URI=URI+'/'+view.splunk().id();
			}
			$.ajax({
				type : "POST",
				async: 'false',
				url : URI,
				success : function(data) {
					if(callback)callback();
				},
				data : 	ko.toJSON(view.splunk()),
				dataType : 'json',
				contentType : 'application/json'
			});
		}
	};
}); 