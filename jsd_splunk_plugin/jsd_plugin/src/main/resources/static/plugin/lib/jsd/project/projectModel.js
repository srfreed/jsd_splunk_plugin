define(['knockout', 'webhook', 'project'], function(ko) {

	var SERVER_URL = '/project';
	
	return {
		id: ko.observable(0),
		projectId : ko.observable(),
		projectName : ko.observable(),
		projectKey : ko.observable(),
		clientKey : ko.observable(),
		
		reset: function(callback){
			var self = this;
			self.projectId(0);
			self.projectName('');
			self.projectKey('');
			self.clientKey('');
			
			if(callback)callback();
			
			return self;
		},
		load: function(id, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/' + id,
				success : function(data) {
					self.id(data.payload.id);
					self.projectId(data.payload.projectId);
					self.projectName(data.payload.projectName);
					self.projectKey(data.payload.projectKey);
					self.clientKey(data.payload.clientKey);
					
					if(callback)callback();
				},
				dataType : 'json',
				contentType : 'application/json'
			});
			return self;
		},
		remove: function(id, callback){
			$.ajax({
				type : "DELETE",
				async: 'false',
				url : SERVER_URL + '/' + id,
				success : function(data) {
					if(callback)callback();
				},				
				dataType : 'json',
				contentType : 'application/json'
			});
			return true;
		},
		findAll: function(view, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey,
				success : function(data) {
					view.projects.removeAll();
					ko.utils.arrayMap(data.payload, function(p) {
						view.projects.push(p);
					});
					if(callback)callback();
				},				
				contentType : 'application/json',
				dataType : 'json'
			});
		},
		save: function(view, callback){
			delete view.projects().__moduleId__;
			$.ajax({
				type : "POST",
				async: 'false',
				url : SERVER_URL + '/',
				success : function(data) {
					if(callback)callback();
				},
				data : ko.toJSON(view.project()),
				dataType : 'json',
				contentType : 'application/json'
			});
		}
	};
}); 