define(['knockout', 'webhook', 'project'], function(ko) {

	var SERVER_URL = '/projectRegistration';

	return {
		id: ko.observable(),
		projectId : ko.observable(),
		splunkId : ko.observable(),
		webhooks : ko.observableArray([]),
		project : ko.observable(),
		splunk : ko.observable(),
		
		reset: function(callback){
			var self = this;
			self.id(0);
			self.projectId('');
			self.project('');
			self.splunkId('');
			self.splunk('');
			self.webhooks([]);
			
			if(callback)callback();
			
			return self;
		},
		load: function(id, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey + '/' + id,
				success : function(data) {
					self.id(data.payload.id);
					self.projectId(data.payload.project.projectId);
					self.splunkId(data.payload.splunk.spunkId);
					self.webhooks(data.payload.webhooks);
					
					if(callback)callback();
				},
				dataType : 'json',
				contentType : 'application/json'
			});
			return self;
		},
		remove: function(id, callback){
			$.ajax({
				type : "DELETE",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey + '/' + id,
				success : function(data) {
					if(callback)callback();
				},				
				dataType : 'json',
				contentType : 'application/json'
			});
			return true;
		},
		findAll: function(view, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/clientKey/' +clientKey,
				success : function(data) {
					view.registrations.removeAll();
					ko.utils.arrayMap(data.payload, function(p) {
						view.registrations.push(p);
					});
					if(callback)callback();
				},
				dataType : 'json',
				contentType : 'application/json'
			});
		},
		save: function(view, callback){
			delete view.registration().__moduleId__;

			var URI = SERVER_URL + '/clientKey/' +clientKey;
			if(view.registration().id()!='' && view.registration().id()!='0'){
				
				URI=URI+'/'+view.registration().id();
			}
			
			$.ajax({
				type : "POST",
				async: 'false',
				url : URI,
				success : function(data) {
					if(callback)callback();
				},
				xhrFields: {
					withCredentials: true
				},
				data : 	ko.toJSON(view.registration()),
				dataType : 'json',
				contentType : 'application/json'
			});
		}
	};
}); 