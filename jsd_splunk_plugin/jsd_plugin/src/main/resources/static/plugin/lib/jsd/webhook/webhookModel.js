define(['knockout'], function(ko) {

	var SERVER_URL = '/webhooks';
	
	return {
		id: ko.observable(0),
		command : ko.observable(),
		
		reset: function(callback){
			var self = this;
			
			self.id(0);
			self.command('');
			
			if(callback)callback();
			
			return self;
		},		
		findAll: function(view, callback){
			var self = this;
			$.ajax({
				type : "GET",
				async: 'false',
				url : SERVER_URL + '/',
				success : function(data) {
					view.allWebHooks.removeAll();
					ko.utils.arrayMap(data, function(p) {
						view.allWebHooks.push(p);
						if(callback)callback();
					});
				},
				dataType : 'jsonp',
				contentType : 'application/json'
			});
		}
	};
}); 